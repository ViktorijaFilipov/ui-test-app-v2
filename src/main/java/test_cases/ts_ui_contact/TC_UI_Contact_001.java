package test_cases.ts_ui_contact;

import org.testng.annotations.Test;
import test_utils.BaseTestCase;
import test_utils.ExtentReports.ExtentTestManager;

import static test_utils.ExtentReports.ExtentTestManager.getTest;

/**
 * Created by vpetrou on 1/3/2018.
 */
public class TC_UI_Contact_001 extends BaseTestCase {

    @Test
    public void TC_UI_Contact_001() {
        getTest().setDescription(TC_Desc.get(getTestCaseId()));
        page.loginPage()
                .clickRegister();
        page.registerPage()
                .verifyPageOpens()
                .register("bill@testflock.org", "1234", "Bill", "Petrou");
        page.menu()
                .verifyMenuContentWhenNotLogin();
        page.loginPage()
                .login("bill@testflock.org", "1234");
        page.menu()
                .goToCreateNewContact();
        page.createContactPage()
                .verifyPageOpens()
                .create("Bill", "Makedonias 120", "Thes/niki", "2310123456", "testflockorg@gmail.com");
        page.listOfContactsPage()
                .verifyPageOpens()
                .verifyTableContains("Bill");
        page.menu()
                .logout();
    }

}
